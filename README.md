1. Exista branch master, cu un commit initial ce contine conditia laboratorului cu denumirea readme.txt.
2. Nimeni nu face push pe master.
3. Fiecare membru lucreaza pe branch-ul sau propriu.
4. Se creaza un fisier membri.txt daca nu a fost creat anterior
5. Pe primul rind va fi prenumele fiecarui membru a echipei
6. Pe al doilea rind va fi numele fiecarui membru a echipei
7. Fiecare membru face pull request, si adauga la review pe ceilalti membri.
8. Inainte de a face pull request branch-ul propriu trebuie sa contina ultima versiune a branch-ului master. 
	(Analizati comanda git rebase)
9. Fiecare pull request trebuie sa contina un singur commit
10. Ceilalti membri trebuie sa dea accept la pull request daca totul este in regula.
11. Dupa ce ultimul membru a facut merge dintre branch-ul sau si che
	master, fiecare membru isi muta numele de familie in fisierul nume.txt	
12. Arborele din source tree trebuie sa fie o linie dreapta